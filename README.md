## To use this web application you need to have 
* Python version > 3.6 https://www.python.org/downloads/release/python-380/
* Mysql https://www.mysql.com/downloads/


## After cloning this repo 
1. Create a mysql database locally `Create database 'databaseName'`
1. Make sure to have django version 4 installed `pip install Django`
1. Install third party modules  `pip install django-crispy-forms` `pip install django-environ`
1. Head to exam/exam and create a .env file with the values below
```
 DATABASE_USER= your database user name usually it is root
 DATABASE_PASS=your mysql password
 DATABASE_NAME=the name of the database you created
```
1. Create a migrations using this command `python manage.py makemigrations`
1. Apply these migration using this command `python manage.py migrate`
1. Create a super user run `python manage.py createsuperuser`
1. finllay run the manage.py file in the exam directory `python manage.py runserver` and head to http://localhost:8000/login/