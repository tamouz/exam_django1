from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.


class Vacation(models.Model):
    description = models.CharField(max_length=200)
    dateFrom = models.DateTimeField()
    dateTo = models.DateTimeField()
    employee = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.description

    def dateEditable(self):
        now = timezone.now()
        return self.dateFrom > now

    # def save(self, *args, **kwargs):
    #     # Current datetime for the timezone of your variable
    #     if self.dateTo < datetime.datetime.today() or self.dateFrom < datetime.datetime.today():
    #         raise ValidationError("The date cannot be in the past!")
    #     super(Vacation, self).save(*args, **kwargs)

    def duration(self):
        return abs((self.dateFrom - self.dateTo).days)
