from django import forms
from .models import Vacation
from django.core.exceptions import ValidationError
from django.utils import timezone


class VacationForm(forms.ModelForm):
    dateFrom = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker1'
        }))
    dateTo = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker2'
        }))
    description = forms.CharField(max_length=200, widget=forms.TextInput(attrs={
        'class': 'form-control'
    }))

    def clean(self):
        cleaned_data = super().clean()
        date1 = cleaned_data['dateFrom']
        date2 = cleaned_data['dateTo']
        if date1 > date2:
            self.add_error(
                'dateTo', "starting date is longer than ending date")
        if date1 < timezone.now():
            self.add_error('dateFrom', 'Date cannot be in the past')

    class Meta:
        model = Vacation
        exclude = ['employee']   # Will be taken from the request
