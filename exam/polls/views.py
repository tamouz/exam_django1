import re
from .models import Vacation
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import VacationForm
from django.core import serializers

# Create your views here.


@login_required
def vacation(request):
    if request.method == "POST":
        form = VacationForm(request.POST)
        if form.is_valid():
            try:
                instance = form.save(commit=False)
                instance.employee = request.user
                instance.save()
                return redirect('/show')
            except Exception:
                return redirect('/show')
    else:
        form = VacationForm(initial={'employee': request.user.id})
    return render(request, 'employee/index.html', {'form': form})


@login_required
def show(request):
    # vacations = serializers.serialize(
    #     "json", Vacation.objects.filter(employee=request.user.id))
    vacations = Vacation.objects.filter(employee=request.user.id)
    return render(request, 'employee/show.html', {'vacations': vacations})


@login_required
def edit(request, id):
    vacation = Vacation.objects.get(id=id)
    if not vacation.dateEditable():
        return redirect('/show')
    return render(request, 'employee/edit.html', {'form': vacation})


@login_required
def update(request, id):
    vacation = Vacation.objects.get(id=id)
    if not vacation.dateEditable():
        return redirect('/show')
    form = VacationForm(request.POST, instance=vacation)
    if form.is_valid():
        form.save()
        return redirect('/show')
    return render(request, 'employee/edit.html', {'form': vacation, 'errors': form.errors})


@login_required
def destroy(request, id):
    vacation = Vacation.objects.get(id=id)
    if vacation.dateEditable():
        vacation.delete()
    return redirect('/show')


# login
@login_required
def logout_view(request):
    logout(request)


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/show')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})
